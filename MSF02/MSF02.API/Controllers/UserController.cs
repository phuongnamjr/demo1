﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MSF02.Data.Repositories;
using MSF02.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSF02.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _UserRepository;
        private readonly IWebHostEnvironment _en;
        public UserController(IUserRepository userRepository)
        {
            _UserRepository = userRepository;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var list = _UserRepository.GetAll();
                return Ok(new ApiRespone
                {
                    Success = true,
                    Message = "Get Seccess",
                    Data = list
                });
            }
            catch
            {
                return BadRequest(new ApiRespone
                {
                    Success = false,
                    Message = "Get Fail"
                });
            }
        }
        [HttpPost]
        public async Task<IActionResult> Create(UserViewModel model)
        {
            try
            {
                var data = await _UserRepository.Create(model);
                return Ok(new ApiRespone
                {
                    Success = true,
                    Message = "Add success",
                    Data = data

                });
            }
            catch
            {
                return BadRequest(new ApiRespone
                {
                    Success = false,
                    Message = "Add fail"
                });
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
            try
            {
                var data = await _UserRepository.GetById(id);
                if (data != null)
                {
                    return Ok(new ApiRespone
                    {
                        Success = true,
                        Message = "Get Success",
                        Data = data
                    });
                }
                else return BadRequest(new ApiRespone
                {
                    Success = false,
                    Message = "Get Fail"
                });
            }
            catch
            {
                return BadRequest(new ApiRespone
                {
                    Success = false,
                    Message = "Get Fail",

                });
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _UserRepository.Delete(id);
                return Ok(new ApiRespone
                {
                    Success = true,
                    Message = "Delete success"
                });
            }
            catch
            {
                return BadRequest(new ApiRespone
                {
                    Success = false,
                    Message = "Delete fail"
                });
            }
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, UserViewModel model)
        {
            if (id != model.ID)
            {
                return BadRequest(new ApiRespone
                {
                    Success = false,
                    Message = "Error"
                });
            }
            else
            {
                try
                {
                    await _UserRepository.Update(id, model);
                    return Ok(new ApiRespone
                    {
                        Success = true,
                        Message = "Update success"
                    });
                }
                catch
                {
                    return BadRequest(new ApiRespone
                    {
                        Success = false,
                        Message = "Update fail"
                    });
                }
            }

        }
    }
}
