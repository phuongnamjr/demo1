﻿using Microsoft.EntityFrameworkCore;
using MSF02.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF02.Data
{
    public class MyDbContext: DbContext
    {
        public MyDbContext() { }
        public MyDbContext(DbContextOptions dbContext) : base(dbContext)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                options.UseSqlServer("Data Source=PHUONGNAM\\SQLEXPRESS;Initial Catalog=MSF02;User ID=sa;Password=phuongnam@25");
            }
        }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder model)
        {

        }

    }
}
