﻿using MSF02.Data.ViewModels;
using MSF02.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;



namespace MSF02.Data.Repositories
{
    public interface IUserRepository
    {
        List<User> GetAll();
        Task<User> GetById(int id);
        Task<int> Create(UserViewModel model);
        Task<int> Update(int id, UserViewModel model);
        Task<int> Delete(int id);

    }
    public class UserRepository : IUserRepository
    {
        private readonly MyDbContext _db;
        
        public UserRepository(MyDbContext db)
        {
            _db = db;
        }
        public async Task<int> Create(UserViewModel model)
        {
            var user = new User
            {
                Name = model.Name,
                Email = model.Email,
                Password = model.Password
            };
            _db.Users.Add(user);
            await _db.SaveChangesAsync();
            return user.ID;
        }

        public async Task<int> Delete(int id)
        {
            var result = await _db.Users.FindAsync(id);
            _db.Users.Remove(result);
            await _db.SaveChangesAsync();
            return result.ID;
        }

        public List<User> GetAll()
        {
            var result = _db.Users.Select(u => new User
            {
                ID = u.ID,
                Name = u.Name,
                Email = u.Email,
                Password = u.Password
            });
            
            
            return  result.ToList();
            
        }

        public async Task<User> GetById(int id)
        {
            var result = await _db.Users.FindAsync(id);
            return result;
        }

        public async Task<int> Update(int id, UserViewModel model)
        {
            var user = await _db.Users.FindAsync(id);
            user.Name = model.Name;
            user.Email = model.Email;
            user.Password = model.Password;
            
            await _db.SaveChangesAsync();
            return user.ID;
        }

    }
}
