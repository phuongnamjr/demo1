﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF02.Data.ViewModels
{
    public class ApiRespone
    {
            public bool Success { get; set; }
            public string Message { get; set; }
            public object Data { get; set; }
        
    }
}
